### Start application
```
git clone https://anton_eliseev@bitbucket.org/anton_eliseev/scraper-reserv.git
cd scraper-reserv
mvn spring-boot:run
```
See scraping results:
```
http://localhost:8080
```

### Parameters:
see **application.properties** to set up SQLite db file and other properties
```
spring.datasource.url               - current SQLite file (scr-scrapped.db - file with already scrapped full data)
google.search.job.cron              - cron expression to start google search job
google.search.job.companies.count   - companies count to process in one google search job
google.search.job.links.by.company  - count of top google search links to store
```

### Google search comments:
Google prohibits the usage of its search engine by automated means (scripts, bots etc.) and sometimes blocks suspicious requests.
This block should expire shortly after those requests stop.

If this application detects such block (google offers to solve captcha) it stop sending queries,
and set up flag GOOGLE_BAN.
Application user should wait until block is gone, and manually start making queries
by clicking link 'remove google ban flag' on http://localhost:8080 page
