package com.aeliseev.scraper.web;

import com.aeliseev.scraper.entity.Category;
import com.aeliseev.scraper.entity.ServiceCompany;
import com.aeliseev.scraper.entity.StateKey;
import com.aeliseev.scraper.service.CategoryService;
import com.aeliseev.scraper.service.ScrapStateService;
import com.aeliseev.scraper.service.ServiceCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
public class WebController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ServiceCompanyService scService;

    @Autowired
    private ScrapStateService ssService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView mav = new ModelAndView("home");
        mav.addObject("googleBan", ssService.getScrapState(StateKey.GOOGLE_BAN).getValue());
        mav.addObject("categories", categoryService.getCategories());
        return mav;
    }

    @RequestMapping(value = "/remove-ban-flag", method = RequestMethod.GET)
    public ModelAndView removeBanFlag() {
        ssService.saveScrapState(StateKey.GOOGLE_BAN, Boolean.FALSE);
        return new ModelAndView("remove-ban");
    }

    @RequestMapping(value = "/category/{categoryId}", method = RequestMethod.GET)
    public ModelAndView category(@PathVariable Long categoryId) {
        ModelAndView mav = new ModelAndView("category");
        Optional<Category> catOpt = categoryService.findById(categoryId);
        if (catOpt.isPresent()) {
            mav.addObject("category", catOpt.get());
            mav.addObject("serviceCompanies", scService.getServiceCompaniesByCategory(catOpt.get()));
        }
        return mav;
    }

    @RequestMapping(value = "/service-company/{serviceCompanyId}", method = RequestMethod.GET)
    public ModelAndView serviceCompany(@PathVariable Long serviceCompanyId) {
        ModelAndView mav = new ModelAndView("service-company");
        Optional<ServiceCompany> scOpt = scService.findById(serviceCompanyId);
        if (scOpt.isPresent()) {
            mav.addObject("serviceCompany", scOpt.get());
            mav.addObject("googleSearches", scService.getGoogleSearchesByServiceCompany(scOpt.get()));
        }
        return mav;
    }
}
