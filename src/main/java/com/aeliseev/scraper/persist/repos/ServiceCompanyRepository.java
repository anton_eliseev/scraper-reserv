package com.aeliseev.scraper.persist.repos;

import com.aeliseev.scraper.entity.Category;
import com.aeliseev.scraper.entity.ServiceCompany;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ServiceCompanyRepository extends JpaRepository<ServiceCompany, Long> {

    List<ServiceCompany> findByNameAndCategory(String name, Category category);

    @Query("SELECT sc FROM ServiceCompany sc WHERE sc.googleSearches IS EMPTY")
    List<ServiceCompany> findWithoutGoogleSearch(Pageable pageable);

    List<ServiceCompany> findByCategory(Category category);
}
