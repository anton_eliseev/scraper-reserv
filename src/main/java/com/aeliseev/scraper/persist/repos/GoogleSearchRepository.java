package com.aeliseev.scraper.persist.repos;

import com.aeliseev.scraper.entity.GoogleSearch;
import com.aeliseev.scraper.entity.ServiceCompany;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GoogleSearchRepository extends JpaRepository<GoogleSearch, Long> {

    List<GoogleSearch> findByServiceCompany(ServiceCompany serviceCompany);
}