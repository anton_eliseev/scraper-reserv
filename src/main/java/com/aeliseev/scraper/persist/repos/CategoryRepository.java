package com.aeliseev.scraper.persist.repos;

import com.aeliseev.scraper.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    List<Category> findByUrl(String url);
}
