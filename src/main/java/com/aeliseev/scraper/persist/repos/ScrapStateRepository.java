package com.aeliseev.scraper.persist.repos;

import com.aeliseev.scraper.entity.ScrapState;
import com.aeliseev.scraper.entity.StateKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScrapStateRepository extends JpaRepository<ScrapState, Long> {

    ScrapState findByKey(StateKey key);
}
