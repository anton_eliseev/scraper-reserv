package com.aeliseev.scraper.scr.load;

import com.aeliseev.scraper.service.UserAgentService;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URLEncoder;

@Service
public class ResourceLoader {

    @Autowired
    private UserAgentService userAgentService;

    public Document load(String url) throws IOException {
        Request request = new Request.Builder()
            .url(url)
            .header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
            .header("Referer", "https://www.google.com/search")
            .build();
        OkHttpClient httpClient = new OkHttpClient().newBuilder().build();
        Response response = httpClient.newCall(request).execute();
        return Jsoup.parse(response.body().string());
    }

    public Document loadGoogleSearch(String query) throws IOException {
        Request request = new Request.Builder()
            .url("https://www.google.com/search?q=" + URLEncoder.encode(query, "UTF-8"))
            .header("User-Agent", userAgentService.getRandomUserAgent())
            .header("Referer", "https://www.google.com/search")
            .build();
        OkHttpClient.Builder clientBuilder = new OkHttpClient().newBuilder();
        OkHttpClient httpClient = clientBuilder.build();
        Response response = httpClient.newCall(request).execute();
        return Jsoup.parse(response.body().string());
    }
}
