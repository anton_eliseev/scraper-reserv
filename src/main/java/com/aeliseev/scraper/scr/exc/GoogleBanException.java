package com.aeliseev.scraper.scr.exc;

public class GoogleBanException extends Exception {
    public GoogleBanException(String message) {
        super(message);
    }
}
