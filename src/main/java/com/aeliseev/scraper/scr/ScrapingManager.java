package com.aeliseev.scraper.scr;

import com.aeliseev.scraper.entity.StateKey;
import com.aeliseev.scraper.scr.exc.GoogleBanException;
import com.aeliseev.scraper.service.ScrapService;
import com.aeliseev.scraper.service.ScrapStateService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ScrapingManager {

    @Autowired
    private ScrapService scrapService;

    @Autowired
    private ScrapStateService scrapStateService;

    @SneakyThrows
    public void startScrape() {
        log.info("Start scraping process");
        scrapService.processSite();
        log.info("Finish scraping process");
    }

    @Scheduled(cron = "${google.search.job.cron}")
    public void startSearchGoogleJob() {
        try {
            scrapService.googleSearchJob();
        }
        catch (GoogleBanException exc) {
            scrapStateService.saveScrapState(StateKey.GOOGLE_BAN, Boolean.TRUE);
        }
    }
}
