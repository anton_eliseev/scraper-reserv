package com.aeliseev.scraper.service;

import com.aeliseev.scraper.entity.Category;
import com.aeliseev.scraper.entity.GoogleSearch;
import com.aeliseev.scraper.entity.ServiceCompany;
import com.aeliseev.scraper.persist.repos.GoogleSearchRepository;
import com.aeliseev.scraper.persist.repos.ServiceCompanyRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;

@Service
public class ServiceCompanyService {

    @Autowired
    private ServiceCompanyRepository scRepository;

    @Autowired
    private GoogleSearchRepository gsRepository;

    @Transactional
    public ServiceCompany save(ServiceCompany sc) {
        return scRepository.save(sc);
    }

    @Transactional
    public ServiceCompany saveOrUpdate(ServiceCompany scToSave) {
        List<ServiceCompany> foundList = scRepository.findByNameAndCategory(scToSave.getName(), scToSave.getCategory());
        if (!CollectionUtils.isEmpty(foundList)) {
            ServiceCompany foundSc = foundList.get(0);
            BeanUtils.copyProperties(scToSave, foundSc, "id", "name", "category");
            return save(foundSc);
        }
        else {
            return save(scToSave);
        }
    }

    @Transactional
    public GoogleSearch save(GoogleSearch gs) {
        return gsRepository.save(gs);
    }

    public List<ServiceCompany> getWithoutGoogleSearch(int limit) {
        return scRepository.findWithoutGoogleSearch(
            PageRequest.of(0, limit, Sort.by(Sort.Direction.ASC, "id"))
        );
    }

    public List<ServiceCompany> getServiceCompaniesByCategory(Category category) {
        return scRepository.findByCategory(category);
    }

    public Optional<ServiceCompany> findById(Long serviceCompanyId) {
        return scRepository.findById(serviceCompanyId);
    }

    public List<GoogleSearch> getGoogleSearchesByServiceCompany(ServiceCompany serviceCompany) {
        return gsRepository.findByServiceCompany(serviceCompany);
    }

}
