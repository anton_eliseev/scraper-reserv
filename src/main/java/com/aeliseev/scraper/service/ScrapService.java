package com.aeliseev.scraper.service;

import com.aeliseev.scraper.entity.*;
import com.aeliseev.scraper.scr.exc.GoogleBanException;
import com.aeliseev.scraper.scr.load.ResourceLoader;
import com.diffplug.common.base.Errors;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Slf4j
public class ScrapService {

    public static final String SITE_TO_SCRAPE = "https://www.pegaxis.com";
    private static final String SERVICES_PAGE = "/services";

    private static final String CATEGORY_BLOCK_HINT = "div[class*=service-title]";
    private static final String CATEGORY_LINK_HINT = "a[class*=service_list_title]";

    private static final String COMPANY_BLOCK_HINT = "div[class*=company-information]";
    private static final String COMPANY_IMAGE_HINT = "div[class*=company-picture] > img";
    private static final String COMPANY_NAME_HINT = "div[class*=company-name] > a";

    private static final String GOOGLE_SEARCH_LINK_HINT = "div.g .r>a";

    private static final String GOOGLE_SEARCH_TEST_HINT_1 = "script[src*=https://www.google.com/recaptcha/api.js]";
    private static final String GOOGLE_SEARCH_TEST_HINT_2 = "div:contains(Our systems have detected unusual traffic from your computer network.)";

    @Autowired
    private ResourceLoader loader;

    @Autowired
    private ScrapStateService scrapStateService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ServiceCompanyService scService;

    @Value("${google.search.job.companies.count}")
    private Integer gsCompaniesCount;

    @Value("${google.search.job.links.by.company}")
    private Integer gsLinksByCompany;

    @SneakyThrows
    public void processSite() {

        ScrapState mainState = scrapStateService.getScrapState(StateKey.MAIN_SCRAPING_COMPLETE);

        if (BooleanUtils.isNotTrue(mainState.getValue())) {
            Document doc = loader.load(SITE_TO_SCRAPE + SERVICES_PAGE);
            Elements categoryBlocks = doc.select(CATEGORY_BLOCK_HINT);
            categoryBlocks.stream().sequential()
                .filter(Objects::nonNull)
                .forEach(block -> {
                    Element categoryLink = block.select(CATEGORY_LINK_HINT).first();
                    Category category = new Category();
                    category.setName(categoryLink.text());
                    category.setUrl(categoryLink.attr("href"));
                    category = categoryService.saveOrUpdate(category);

                    processCategory(category);
                });

            scrapStateService.saveScrapState(StateKey.MAIN_SCRAPING_COMPLETE, Boolean.TRUE);
        }
    }

    /*
        Unfortunately sqlite can be written only by one thread
        @Async
    */
    public void processCategory(Category category) {
        final AtomicInteger i = new AtomicInteger(1);
        try {
            Document doc = loader.load(SITE_TO_SCRAPE + category.getUrl());

            Elements companies = doc.select(COMPANY_BLOCK_HINT);
            companies.stream().filter(Objects::nonNull).forEach(block -> {
                ServiceCompany sc = new ServiceCompany();

                Element nameBlock = block.select(COMPANY_NAME_HINT).first();
                sc.setName(nameBlock.text());

                sc.setCategory(category);

                Element imageBlock = block.select(COMPANY_IMAGE_HINT).first();
                sc.setImageUrl(imageBlock.attr("src"));

                sc = scService.saveOrUpdate(sc);
                i.getAndAdd(1);
            });
        }
        catch (IOException exc) {
            log.error("Error while processing category " + category.getUrl(), exc);
        }
        log.info(String.format("Processed category [%s], added [%d] companies", category.getName(), i.intValue()));
    }

    public void googleSearchJob() throws GoogleBanException {

        ScrapState mainState = scrapStateService.getScrapState(StateKey.MAIN_SCRAPING_COMPLETE);
        ScrapState googleBanState = scrapStateService.getScrapState(StateKey.GOOGLE_BAN);

        if (BooleanUtils.isTrue(mainState.getValue()) && BooleanUtils.isNotTrue(googleBanState.getValue())) {
            log.info("Start search google scheduled task");
            final AtomicInteger i = new AtomicInteger(0);
            List<ServiceCompany> scList = scService.getWithoutGoogleSearch(gsCompaniesCount);
            if (!CollectionUtils.isEmpty(scList)) {
                scList.stream().forEach(Errors.rethrow().wrap(sc -> {
                    i.addAndGet(doGoogleSearchForServiceCompany(sc));
                }));
            }
            log.info(String.format("Google search job added %d links.", i.get()));
        }
    }

    private int doGoogleSearchForServiceCompany(ServiceCompany sc) throws GoogleBanException {
        final AtomicInteger i = new AtomicInteger(0);
        try {
            Document gDoc = loader.loadGoogleSearch(sc.getName());

            Element e1 = gDoc.select(GOOGLE_SEARCH_TEST_HINT_1).first();
            Element e2 = gDoc.select(GOOGLE_SEARCH_TEST_HINT_2).first();
            if (e1 != null && e2 != null) {
                throw new GoogleBanException("Looks like google suspects robot.");
            }

            Elements links = gDoc.select(GOOGLE_SEARCH_LINK_HINT);
            links.stream()
                // skip Ads/news/etc.
                .filter(Errors.rethrow().wrapPredicate(link -> getGoogleReturnURL(link).startsWith("http")))
                .limit(gsLinksByCompany)
                .forEach(Errors.rethrow().wrap(link -> {
                    GoogleSearch gs = new GoogleSearch();
                    gs.setTitle(link.text());
                    gs.setUrl(getGoogleReturnURL(link));
                    gs.setServiceCompany(sc);
                    gs = scService.save(gs);
                    i.getAndAdd(1);
                }));
        }
        catch (IOException exc) {
            log.error("Error while loading google search " + sc.getName(), exc);
        }
        return i.get();
    }

    private String getGoogleReturnURL(Element link) throws IOException {
        String url = link.attr("href");
        String urlSimplified = url;
        if (url.contains("url?q=") && url.contains("&")) {
            // for 'example bot' User-aAgent google returns URLs in format "http://www.google.com/url?q=<url>&sa=U&ei=<someKey>".
            urlSimplified = url.substring(url.indexOf('=') + 1, url.indexOf('&'));
        }
        return URLDecoder.decode(urlSimplified, "UTF-8");
    }
}
