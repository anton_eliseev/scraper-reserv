package com.aeliseev.scraper.service;

import com.aeliseev.scraper.entity.ScrapState;
import com.aeliseev.scraper.entity.StateKey;
import com.aeliseev.scraper.persist.repos.ScrapStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScrapStateService {

    @Autowired
    private ScrapStateRepository ssRepository;

    public ScrapState getScrapState(StateKey key) {
        return ssRepository.findByKey(key);
    }

    public ScrapState saveScrapState(StateKey key, Boolean value) {
        ScrapState result = getScrapState(key);
        result.setValue(value);
        return ssRepository.save(result);
    }
}
