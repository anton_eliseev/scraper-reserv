package com.aeliseev.scraper.service;

import com.aeliseev.scraper.entity.Category;
import com.aeliseev.scraper.persist.repos.CategoryRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Transactional
    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    @Transactional
    public Category saveOrUpdate(Category categoryToSave) {
        List<Category> foundList = categoryRepository.findByUrl(categoryToSave.getUrl());
        if (!CollectionUtils.isEmpty(foundList)) {
            Category foundCat = foundList.get(0);
            BeanUtils.copyProperties(categoryToSave, foundCat, "id", "url");
            return save(foundCat);
        }
        else {
            return save(categoryToSave);
        }
    }

    public List<Category> getCategories() {
        return categoryRepository.findAll();
    }

    public Optional<Category> findById(Long categoryId) {
        return categoryRepository.findById(categoryId);
    }
}
