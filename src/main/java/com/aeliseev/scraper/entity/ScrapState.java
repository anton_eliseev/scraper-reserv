package com.aeliseev.scraper.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class ScrapState extends AbstractEntity {

    @Enumerated(value = EnumType.STRING)
    private StateKey key;

    private Boolean value;
}
