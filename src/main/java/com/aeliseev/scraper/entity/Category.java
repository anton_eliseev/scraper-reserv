package com.aeliseev.scraper.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Category extends AbstractEntity {
    private String name;
    private String url;
}
