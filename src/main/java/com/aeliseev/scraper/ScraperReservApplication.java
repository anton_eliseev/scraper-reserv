package com.aeliseev.scraper;

import com.aeliseev.scraper.scr.ScrapingManager;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.*;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@SpringBootApplication
@EnableTransactionManagement
@EnableAsync
@EnableScheduling
public class ScraperReservApplication implements AsyncConfigurer, SchedulingConfigurer {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(ScraperReservApplication.class, args);
		context.getBean(ScrapingManager.class).startScrape();
	}

	@Bean(destroyMethod="shutdown")
	public Executor scheduledTaskExecutor() {
		return Executors.newScheduledThreadPool(1);
	}

	@Override
	public Executor getAsyncExecutor() {
		return Executors.newFixedThreadPool(10);
	}

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return new SimpleAsyncUncaughtExceptionHandler();
	}

	@Override
	public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
		scheduledTaskRegistrar.setScheduler(scheduledTaskExecutor());
	}
}