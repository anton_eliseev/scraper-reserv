CREATE TABLE google_search
(
	id INTEGER NOT NULL PRIMARY KEY,
	title TEXT,
	url TEXT,
	service_company_id INTEGER
);
