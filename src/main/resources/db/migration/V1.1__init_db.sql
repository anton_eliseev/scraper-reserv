CREATE TABLE category
(
	id INTEGER NOT NULL PRIMARY KEY,
	name TEXT,
	url TEXT
);

CREATE TABLE service_company
(
	id INTEGER NOT NULL PRIMARY KEY,
	name TEXT,
	image_url TEXT,
	category_id INTEGER
);

